#!/bin/bash

echo
echo "Bienvenido al desinstalador de paquetería snap para Ubuntu"
echo
sleep 1s
echo
echo "Listando paquetes snap instalados en el sistema..."
sleep 3s
echo
snap list || { echo "Error: snap no está instalado o hay un problema con snapd."; exit 1; }
echo
echo "Deteniendo servicios de snapd..."
sleep 1s
echo
sudo systemctl stop snapd.service
sudo systemctl disable snapd.service snapd.socket snapd.seeded.service
echo
echo "Desinstalando snaps..."
sleep 1s
echo
for package in firefox snap-store gtk-common-themes gnome-3-38-2004 snapd-desktop-integration core20 bare snapd; do
    sudo snap remove "$package" || echo "Error eliminando $package o no está instalado."
done
echo
echo "Eliminando la cache de snap..."
sleep 1s
echo
sudo rm -rf /var/cache/snapd/
echo
echo "Desinstalando snapd..."
echo
sudo apt autoremove --purge -y snapd || { echo "Error al desinstalar snapd"; exit 1; }
echo
echo "Eliminando directorios de snap del sistema..."
rm -rf ~/snap
sudo rm -rf /snap /var/snap /var/lib/snapd
echo
echo "Modificando preferencias del gestor de paquetes para evitar snaps en el futuro..."
sleep 1s
echo
if [ -f nosnap.pref ]; then
    sudo cp nosnap.pref /etc/apt/preferences.d/
    sudo apt update
else
    echo "Advertencia: nosnap.pref no encontrado. Saltando este paso."
fi
echo
echo "Instalando Firefox en formato DEB convencional..."
echo
sudo touch /etc/apt/preferences.d/mozillateamppa
echo "Package: firefox*" | sudo tee /etc/apt/preferences.d/mozillateamppa
echo "Pin: release o=LP-PPA-mozillateam" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
echo "Pin-Priority: 501" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
sudo add-apt-repository -y ppa:mozillateam/ppa
sudo apt update -y
sudo apt install -y firefox
echo
echo "Instalando el centro de software de GNOME..."
sleep 1s
echo
sudo apt install -y gnome-software
echo
echo "¡Hecho! Por favor, reinicia el sistema."
echo